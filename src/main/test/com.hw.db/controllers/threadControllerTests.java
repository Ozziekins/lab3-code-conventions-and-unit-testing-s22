package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.internal.verification.Times;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class threadControllerTests {
    private Thread newThread;
    private String slug = "example";
    private String id = "2";

    @BeforeEach
    @DisplayName("Testing the creation of the thread")
    void testCreateThread() {
        newThread = new Thread("personName", new Timestamp(0), "forum", "message", slug, "hello topic", 43);
    }

    @Test
    @DisplayName("Checking id and slug")
    void testCheckIdSlug() {
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();
            threadMock.when(() -> ThreadDAO.getThreadById(Integer.parseInt(id))).thenReturn(newThread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(newThread);
            assertEquals(newThread, controller.CheckIdOrSlug(id), "Everything with ID good.");
            assertEquals(newThread, controller.CheckIdOrSlug(slug), "Everything with slug good.");
        }
    }

    @Test
    @DisplayName("Testing the creation of a post")
    void testCreatePost() {
        List<Post> postsThread = Collections.emptyList();
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(newThread);
            
            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(postsThread), controller.createPost(slug, postsThread));
        }
    }

    @Test
    @DisplayName("Testing the getting of a post")
    void testGetPost() {
        List<Post> postsThread = Collections.emptyList();
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();

            threadMock.when(() -> ThreadDAO.getPosts(newThread.getId(), 200, 2, null, false)).thenReturn(postsThread);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(newThread);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(postsThread), controller.Posts(slug, 200, 2, null, false));
        }
    }

    @Test
    @DisplayName("Testing the changing of a thread")
    void testChangeThread() {
        Thread changeThread = new Thread("personName", new Timestamp(2), "forum", "some message", "someSlug", "hello", 4243);
        changeThread.setId(2);
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug("someSlug")).thenReturn(changeThread);
            threadMock.when(() -> ThreadDAO.getThreadById(2)).thenReturn(newThread);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(newThread), controller.change("someSlug", newThread), "Everything with changing the thread is good.");
        }
    }

    @Test
    @DisplayName("Testing the thread information")
    void testThreadInfo() {
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(newThread);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(newThread), controller.info(slug), "Everything with the thread information is good.");
        }
    }

    @Test
    @DisplayName("Testing the creation of a vote")
    void testCreateVote() {
        Vote vote = new Vote("personName", 5);
        User user = new User("personName", "random@hotmail.com", "Da Hoe", "R D");

        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try(MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(newThread);
                userMock.when(() -> UserDAO.Info("personName")).thenReturn(user);

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(newThread), controller.createVote(slug, vote), "Everything with creating the vote is good.");
            }
        }
    }
}
